from socket import *
import threading
import time
import os

def send(sock):
    while True:
        sendData = input('>>>')
        if sendData == "file":
            #파일 이름 입력
            while True : 
                try: 
                    file_name = input("파일 이름을 입력하세요")
                    #파일의 절대값 주소 생성
                    file_name = os.path.join(os.getcwd(), file_name)
                    f = open(file_name, 'r')
                    break
                except Exception as e : 
                    print("error! ", e)
            data = f.read()
            data = "    " + data
            double_check = input(file_name + "을 전송하시겠습니까? (Y/N)")
            if double_check.lower() == 'y':
                sock.send(data.encode('utf-8'))
                continue
            else:
                continue
        sock.send(sendData.encode('utf-8'))
        if sendData == "/종료":
            break
    sock.close()

def receive(sock):
    while True:
        try:
            recvData = sock.recv(1024)
            data = recvData.decode('utf-8')
            if "    " in data:
                f = open(user + "_" + str(file_count) + "_file_received.txt", 'w')
                f.write(data.strip())
                f.close()
                print("파일을 저장했습니다.")
                data = data.split(":")
                if len(data) == 2:
                    user_name = data[0]
                    msg = data[1]
                continue
            else : 
                data = data.split(":")
                if len(data) == 2:
                    user_name = data[0]
                    msg = data[1]
                    print(user_name,':',msg)
                    continue

        except Exception as e:
            print("Error :", e)
            print(msg)
            sock.close()
            break


port =54321
user = input("접속하고자 하는 이름을 입력해주세요 : ")
file_count = 0
clientSock = socket(AF_INET, SOCK_STREAM)
clientSock.connect(('127.0.0.1', port))
clientSock.sendall(user.encode())

print('접속 완료')

sender = threading.Thread(target=send, args=(clientSock,))
receiver = threading.Thread(target=receive, args=(clientSock,))

sender.start()
receiver.start()

while True:
    time.sleep(1)
    pass