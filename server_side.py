from threading import *
from socket import *

class ServerSocket:
 
    def __init__(self):        
        self.bListen = False       
        self.clients = []
        self.ip = []
        self.threads = []
        self.user_list = []

    def __del__(self):
        self.stop()

    def start(self, ip, port):
        ## 서버 소켓열고, ip와 포트 bind하고 스레드 열어주는 역할
        self.server = socket(AF_INET, SOCK_STREAM)            
 
        try:
            self.server.bind( (ip, port))
        except Exception as e:
            print('Bind Error : ', e)
            return False
        else:                 
            self.bListen = True
            self.t = Thread(target=self.listen, args=(self.server,))
            self.t.start()
            print('Server Listening...')
            
        return True

    def stop(self):
        ## 서버 끄는 역할
        self.bListen = False
        if hasattr(self, 'server'):            
            self.server.close()            
            print('Server Stop')

 
    def listen(self, server):
        ##새로 접속을 시도하는 유저를 받아주는 역할
        while self.bListen:
            server.listen(5)   
            try:
                client, addr = server.accept()
            except Exception as e:
                print('Accept() Error : ', e)
                break
            else:                
                user_dict = dict()
                user = client.recv(1024).decode('utf-8')

                self.clients.append(client)
                self.ip.append(addr)                
                t = Thread(target=self.receive, args=(addr, client))
                self.threads.append(t)
                user_dict['name'] = user
                user_dict['client'] = client
                user_dict['ip'] = addr
                user_dict['thread'] = t
                self.user_list.append(user_dict)
                t.start()   
                self.send("서버 : " + user + "님이 로그인하셨습니다.")
                self.resourceInfo()         
                 
        self.removeAllClients()
        self.server.close()

    def receive(self, addr, client):
        #유저가 보내는 메시지를 받아주는 역할
        while True:            
            try:
                recv = client.recv(1024)         
                msg = str(recv, encoding='utf-8')
                if msg =="/종료":
                    user_name = ""
                    for i in self.user_list:
                        if i['client'] == client:
                            user_name = i['name']
                            break
                    print(user_name + "가 로그아웃 하셨습니다.")
                    self.removeClient(addr, client)
                    self.send('서버 :'+ user_name + "가 로그아웃 하셨습니다.")
                    return
                    break
            except Exception as e:
                user_name = ""
                for i in self.user_list:
                    if i['client'] == client:
                        user_name = i['name']
                        break
                    
                self.removeClient(addr, client)
                self.send('서버 :'+ user_name + "가 비정상적으로 로그아웃 됐습니다.")
                print('Recv() Error :', e, addr)
                return
                break
            else:       
                if "    " in msg: 
                    self.send(msg)
                elif msg:
                    user_name = ""
                    for i in self.user_list:
                        if i['client'] == client:
                            user_name = i['name']
                            break
                    # if msg =="/종료":
                    #     print(user_name + "가 로그아웃 하셨습니다.")
                    #     self.send(user_name + "가 로그아웃 하셨습니다.")
                    #     self.removeClient(addr, client)
                    #     return
                    #     break


                    self.send(user_name+':'+str(msg))
                    print('[RECV From', str(user_name) +']' , addr, msg)
 
        self.removeClient(addr, client)

    def send(self, msg):
        #현재 존자하는 client들에게 메시지를 전송하는 역할
        try:
            for c in self.clients:
                c.send(msg.encode())
        except Exception as e:
            print('Send() Error : ', e)

    def removeClient(self, addr, client):
        #사용자가 비정상적으로 로그아웃되거나, 종료할 경우 로그아웃 시킴
        found = False
        for i in self.user_list:
            if i['client'] == client:
                user_name = i['name']
                self.ip.remove(addr)
                self.clients.remove(client) 
                thread = i['thread']
                i = 0
                for t in self.threads[:]:
                    if t == thread:
                        del(self.threads[i])
                    i +=1
                client.close()
                found = True
                break
        if not found : 
            print("error!!")
        

        self.resourceInfo()

    def removeAllClients(self):
        #모든 client들 다 지우는 역할
        for c in self.clients:
            c.close()
        self.ip.clear()
        self.clients.clear()
        self.threads.clear()
        self.resourceInfo()

    def resourceInfo(self):
        #현재 접속자 상황을 공유하는 역할
        print('Number of Client ip\t: ', len(self.ip) )
        print('Number of Client socket\t: ', len(self.clients) )
        print('Number of Client thread\t: ', len(self.threads) )
        # self.send('서버 :' + 'Number of Client ip -> ' + str(len(self.ip)))
        # self.send('서버 :' + 'Number of Client socket -> '+ str(len(self.clients)))
        # self.send('서버 :' + 'Number of Client thread -> '+ str(len(self.threads)))       


if __name__ == "__main__":
    ip = "127.0.0.1"
    port = 54321
    server = ServerSocket()
    server.start(ip, port)
    