# Socket Programming : Chatting

## How to run_my_code

### 1) environment

- python 3.6이상 설치된 서버 준비

### 2) clone my git

```python
git clone https://gitlab.com/getveryrichet/socketprogramming_multiplechatting.git
```

### 3) how to run server

- server_side.py에서 port와 ip address를 수동으로 수정 후 아래 커맨드를 통해 서버 실행

```python
$ python server_side.py
```

- 본 프로젝트에서는 별도의 UI를 구현하지 않고, 커맨드 창을 UI 화면으로 가정하여 구현됐다.
- 파이썬 3.6이상 버전이 설치된 서버에서 커맨드 라인에

```python
$ python server_side.py
```

- 를 입력시 별도의 코드 수정을 하지 않으면 <127.0.0.1:54321>에 서버 프로세스가 실행된다.

    ![Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled.png](Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled.png)

    - 만약 포트번호 혹은 ip address를 변경하고 싶다면 코드 가장 아래에 서버를 돌릴 <ip>와 <포트 번호>를 지정하면 해당 포트번호로 프로세스가 실행된다.
        - 만약 원격접속을 희망한다면 포트 포워딩 등의 방법을 활용하여 원격에서도 서버에 접속하도록 할 수 있다.
- 실행화면 예시

![Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%201.png](Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%201.png)

### 4) how to run client

- client_side.py에서 port와 ip address를 접속하고자 하는 소켓 서버의 ip와 port로 수정 후 client 실행

```python
$ python client_side.py
```

- 앞서 언급했듯이 별도의 UI  프로그램이 없기 때문에, 원하는 클라이언트 수만큼 cmd창을 열어놓고, 동일한 ip와 port 번호를 client_side.py에서 적어두고 실행

    ![Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%202.png](Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%202.png)

- 실행 화면 예시

![Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%203.png](Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%203.png)

---

## 기능 설명

### 1) TCP를 활용한 통신

- TCP 통신을 위한 socket을 import하여 사용
- 우선 tcp는 위에 모듈을 임포트 한 뒤에 소켓 통신을 할 ip(저는 임의로 123.123.123.123)와 포트를 지정 후에 연결을 하는 방식.
- 메시지를 send할 수 있다. 또한 send가 끝나는 동시에 data를 data_size만큼 받아오게 된다.

### 2) 다중연결 확인

**서버**

![Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%204.png](Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%204.png)

**클라이언트 3명 동시 접속 화면**

![Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%205.png](Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%205.png)

- 접속한 클라이언트
    - 오은택
    - 아피치
    - 클라이언트
- 위 클라이언트는 socket에서 listen할 때 accept함과 동시에  {ip : ~, client : ~, thread : ~,  user:  ~} 형태의 dict를 만들고 user_list라는 리스트에  넣어서 저장하고 트래킹한다.

### 3) 파일 전송

![Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%206.png](Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%206.png)

- **파일을 전송하는 법 : command line에**

    1) file 입력

    2) 같은 디렉토리에 있는 파일명 입력

    3) Y 입력

- 참고 : 본 프로젝트에서는 여러 개의 원격접속 client를 만들 수 있는 서버가 부족하여, 단일 서버에서 로컬로 port만 바꾸어 접속을 시도함.

**receiver가 파일 전송을 구분하는 방법**

- 파일 전송은 data 앞에 "    " 네 개의 whitespace를 두어 전송을하고, 받는 client들도 이를 기준으로 파일인지 아닌지 여부를 판단하여 별도로 처리한다.

**처리방법**

- 오은택이라는 이름으로 접속한 클라이언트가 파일을 전송한 후, 아피치와 클라이언트가 파일을 수신하는 증거화면
- 각 클라이언트들은 .py 프로그램이 실행되는 경로에 <{이름}_{받은파일 갯수}_file_received.txt>라는 명칭으로 파일을 수신하여 저장한다.

### 4) 10회 이상 대화 가능 여부 확인

![Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%207.png](Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%207.png)

- 왼쪽 화면은 서버로,
- 아직 UI를 개발 할 수 있는 방법을 공부하지못해, command로만 구현한 형태라 각각의 client가 보는 화면은 자신이 말한 내용이 반복해서 보여지는 불편함이 있다. 만약 UI를 구현할 수 있었다면 가장 왼쪽의 서버 화면에서 보여지는 대화 내용을 모든 유저들이 볼 수 있도록 할 계획이다.

### 5) 연결 두절 인지

위 대화 도중 아피치가 연결이 중간에 종료되는 시나리오를 실행해 보았다. 

![Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%208.png](Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%208.png)

결과 화면은 아래와 같다.

![Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%209.png](Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%209.png)

정상적으로 종료하는 방법은 채팅창에

"/종료"라고 입력하는 것이다. 클라이언트의 커맨드 라인에 /종료를 입력해보겠다. 

![Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%2010.png](Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%2010.png)

- 클라이언트의 커맨드 라인에서 "/종료"를 입력하니
- 오은택의 화면에서는 아피치가 비정상적으로 로그아웃됐을 때의 문구와는 다르게 "클라이언트가 로그아웃 하셨습니다"라는 문구를 받게 된다.

즉, 자발적으로 나간경우와, 비정상적으로 연결이 끊긴 두 가지 경우 모두 다르게 표시된다.

### 6) 연결 복구

- 본 프로젝트에서는 단일 서버에서 진행하다보니, 클라이언트 접속시 포트 지정이 임의적으로 배정되어 address와 port로는 구분을 하기가 어려운 상황이다.
- 만약 remote access를 가능할 수 있는 환경이었다면 address와 user_name을 기준으로 구분할 수 있겠지만, 본 프로젝트에서는 어렵기 때문에 user_name만을 기준으로 접속할 경우 다시 로그인이 되는 형태로 연결 복구를 구현했다.

![Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%2011.png](Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%2011.png)

- 1번 화면은 비정상적으로 연결이 끊겼었던 아피치가 재접속한 상황
- 2번화면은 자신이 /종료라고 입력한 뒤 재접속한 클라이언트의 모습을 보여주고 있다.

### 7) GUI 개선 희망사항

- UI를 구현할 수 있는 능력이 부족하여 text를 print하는 형태로 구현했지만 아래와 같은 개선 방법이 있다.

### 현재 접속자 수와 ip, socket을 보여주는 화면

![Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%2012.png](Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%2012.png)

- 현재는 서버에서 이와 같은 형태로 출력을 하는 형태인데
- 이를 서버 뿐 아니라 클라이언트에서도 볼 수 있도록 UI  화면을 만들어, 새로운 접속자가 들어오거나 나갔을 경우 이를 트래킹할 수 있는 화면이 필요하다

### 자신이 입력한 내용은 보지 않기

![Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%2013.png](Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%2013.png)

- 위는 "오은택"의 접속화면인데,
- 커맨드라인의 UI상 내가 입력한 부분이 다시 화면에 보여져, 이렇게 커맨드창으로만 대화할경우 내가 말 한 부분이 중복으로 보여져 가독성이 떨어진다.

- 하지만 아래처럼 서버의 화면에서는 중복된 내용이 보여지지 않는다.

![Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%2014.png](Socket%20Programming%20Chatting%2070eaa4098f0f49f884dc9e13226ecc04/Untitled%2014.png)

- 따라서 위 텍스트 부분을 계속해서 모든 사용자들이 볼 수 있는 UI창을 채팅창으로 만들어두면 가독성을 높일 수 있다.

---

---

## Code explanation

### server_side.py 코드 explanation

server_side.py 코드에서 정의된 ServerSocket 클래스의 메소드를 설명하도록 하겠다. 

### start()

```python
def start(self, ip, port):
        ## 서버 소켓열고, ip와 포트 bind하고 스레드 열어주는 역할
        self.server = socket(AF_INET, SOCK_STREAM)            
 
        try:
            self.server.bind( (ip, port))
        except Exception as e:
            print('Bind Error : ', e)
            return False
        else:                 
            self.bListen = True
            self.t = Thread(target=self.listen, args=(self.server,))
            self.t.start()
            print('Server Listening...')
```

- 서버를 running할 때
- socket(address_family, socket type) 세팅은 아래와 같다.
    - 그냥 AF_INET은 IPv4를, AF_INET6는 IPv6를 의미함. 본 프로젝트에서는 AF_INET을 사용
    - 소켓 타입은 TCP 소켓을 위해스트림 타입인 SOCK_STREAM을 사용
- bind(ip, port)를 통해 생성된 소켓의 번호와 실제 address family를 연결해준다.
    - 만약 모든 address를 허용하려면 ip부분에 ""를 입력한다.
- 스레드르 생성하여 listen을 수행하도록 한다.

### stop()

```python
def stop(self):
        ## 서버 끄는 역할
        self.bListen = False
        if hasattr(self, 'server'):            
            self.server.close()            
            print('Server Stop')
```

- 서버를 끄는 함수다.

### Listen()

```python
def listen(self, server):
        ##새로 접속을 시도하는 유저를 받아주는 역할
        while self.bListen:
            server.listen(5)   
            try:
                client, addr = server.accept()
            except Exception as e:
                print('Accept() Error : ', e)
                break
            else:                
                user_dict = dict()
                user = client.recv(1024).decode('utf-8')

                self.clients.append(client)
                self.ip.append(addr)                
                t = Thread(target=self.receive, args=(addr, client))
                self.threads.append(t)
                user_dict['name'] = user
                user_dict['client'] = client
                user_dict['ip'] = addr
                user_dict['thread'] = t
                self.user_list.append(user_dict)
                t.start()   
                self.send("서버 : " + user + "님이 로그인하셨습니다.")
                self.resourceInfo()         
                 
        self.removeAllClients()
        self.server.close()
```

- listen은 서버 소켓에서 쓰이는 부분으로 동시에 몇 개의 접속을 허용할 것인지를 정한다. 본 프로젝트에서는 5를 입력하여 5명까지 동시접속을 허용한다.
- 몇 개의 동시접속을 허용한지 정해둔 뒤에는 계속해서 server에 접속을 요청하는 클라이언트가 없는지 accept()문을 돌려서 확인한다.
    - 이는 누군가가 접속하여 연결됐을 때 결과 값이 return되고 accept()가 실행되면 새로운 소켓과 상대방의 address family를 반환한다.
    - 본 프로젝트에서는 접속하는 클라이언트의 이름과 ip 등을 기록하기 위해 socket에서 listen할 때 accept함과 동시에  {ip : ~, client : ~, thread : ~,  user:  ~} 형태의 dict를 만들고 user_list라는 리스트에  넣어서 저장하고 트래킹한다.
- 또한 만약 accept부분에서 에러가 발생하면 서버에 문제가 발생했다는 것을 의미하기 때문에 실행중인 thread들을 모두 종료하고 서버 소켓을 종료한다.

### receive()

```python
def receive(self, addr, client):
        #유저가 보내는 메시지를 받아주는 역할
        while True:            
            try:
                recv = client.recv(1024)         
                msg = str(recv, encoding='utf-8')
                if msg =="/종료":
                    user_name = ""
                    for i in self.user_list:
                        if i['client'] == client:
                            user_name = i['name']
                            break
                    print(user_name + "가 로그아웃 하셨습니다.")
                    self.removeClient(addr, client)
                    self.send('서버 :'+ user_name + "가 로그아웃 하셨습니다.")
                    return
                    break
            except Exception as e:
                user_name = ""
                for i in self.user_list:
                    if i['client'] == client:
                        user_name = i['name']
                        break
                    
                self.removeClient(addr, client)
                self.send('서버 :'+ user_name + "가 비정상적으로 로그아웃 됐습니다.")
                print('Recv() Error :', e, addr)
                return
                break
            else:       
                if "    " in msg: 
                    self.send(msg)
                elif msg:
                    user_name = ""
                    for i in self.user_list:
                        if i['client'] == client:
                            user_name = i['name']
                            break
                    # if msg =="/종료":
                    #     print(user_name + "가 로그아웃 하셨습니다.")
                    #     self.send(user_name + "가 로그아웃 하셨습니다.")
                    #     self.removeClient(addr, client)
                    #     return
                    #     break

                    self.send(user_name+':'+str(msg))
                    print('[RECV From', str(user_name) +']' , addr, msg)
 
        self.removeClient(addr, client)
```

- 본 함수는 클라이언트에게 메시지를 받게 될 경우 이를 파싱하여 채팅방에 접속한 다른 클라이언트들에게 전달한다.
- 또한 클라이언트의 비정상적인 종료를 탐지한다.
- 또한 클라이언트의 종료 request를 수용하여 서버 쪽에서 클라이언트와의 연결을 끊어주는 역할을 한다.
    - 파일 전송을 의미하는 "    "로 시작되는 문구의 경우 이를 인지하여 모든 클라이언트들에게 파일을 전송한다.

### send()

```python
def send(self, msg):
        #현재 존자하는 client들에게 메시지를 전송하는 역할
        try:
            for c in self.clients:
                c.send(msg.encode())
        except Exception as e:
            print('Send() Error : ', e)
```

- 서버와 소켓을 통해 연결돼있는 모든 클라이언트 들에게 메시지를 전송하는 함수다.
- 파일 전송 뿐 아니라 일반 메시지도 모두 이 send()함수를 통해 전송된다.

### removeClient()

```python
def removeClient(self, addr, client):
        #사용자가 비정상적으로 로그아웃되거나, 종료할 경우 로그아웃 시킴
        found = False
        for i in self.user_list:
            if i['client'] == client:
                user_name = i['name']
                self.ip.remove(addr)
                self.clients.remove(client) 
                thread = i['thread']
                i = 0
                for t in self.threads[:]:
                    if t == thread:
                        del(self.threads[i])
                    i +=1
                client.close()
                found = True
                break
        if not found : 
            print("error!!")
				self.resourceInfo()
```

- addr 와 client를 이용하여 client를 구분하고 해당 사용자가 비정상적으로 종료됐거나 직접 로그인한 경우 서버에서 보관하고 있는 접속중인 클라이언트 목록에서 제거한다.

### removeAllClient()

```python
def removeAllClients(self):
        #모든 client들 다 지우는 역할
        for c in self.clients:
            c.close()
        self.ip.clear()
        self.clients.clear()
        self.threads.clear()
        self.resourceInfo()
```

- 서버 종료를 용이하게 하기 위해 removeClient()를 모든 클라이언트를 대상으로 실행하는 함수

### resourceInfo():

```python
def resourceInfo(self):
        #현재 접속자 상황을 공유하는 역할
        print('Number of Client ip\t: ', len(self.ip) )
        print('Number of Client socket\t: ', len(self.clients) )
        print('Number of Client thread\t: ', len(self.threads) )
        # self.send('서버 :' + 'Number of Client ip -> ' + str(len(self.ip)))
        # self.send('서버 :' + 'Number of Client socket -> '+ str(len(self.clients)))
        # self.send('서버 :' + 'Number of Client thread -> '+ str(len(self.threads)))
```

- 현재 서버와 연결돼있는 클라이언트의 수와 사용되고 있는 자원의 수를 print하는 함수다. (서버 상황을 확인하기 위해 사용)

---

### client_side.py 코드 explanation

- 클라이언트의 경우 위와 같은 형태로 구현돼있다.

### 소켓 세팅

- 먼저 socket을 열고

```python
port =54321
user = input("접속하고자 하는 이름을 입력해주세요 : ")
file_count = 0
clientSock = socket(AF_INET, SOCK_STREAM)
clientSock.connect(('127.0.0.1', port))
clientSock.sendall(user.encode())
```

- 서버와는 다르게 소켓을 열고 bind, listen, accept과정을 거치지 않고 connect가 추가됐따. 또한 본인의 user_name을 알리기 위해 connect되자마자 자신의 user_name을 보내고, 서버는 이를 받아서 등록한다.

### 소켓 송수신

- 연결이 성공한 소켓을 clientSock이라는 변수에 담아둔다.

**전송**

```python
def send(sock):
    while True:
        sendData = input('>>>')
        if sendData == "file":
            #파일 이름 입력
            while True : 
                try: 
                    file_name = input("파일 이름을 입력하세요")
                    #파일의 절대값 주소 생성
                    file_name = os.path.join(os.getcwd(), file_name)
                    f = open(file_name, 'r')
                    break
                except Exception as e : 
                    print("error! ", e)
            data = f.read()
            data = "    " + data
            double_check = input(file_name + "을 전송하시겠습니까? (Y/N)")
            if double_check.lower() == 'y':
                sock.send(data.encode('utf-8'))
                continue
            else:
                continue
        sock.send(sendData.encode('utf-8'))
        if sendData == "/종료":
            break
    sock.close()
```

아래의 3가지 경우를 담당한다.

- 전송의 경우 data의 시작이 "    "로  시작될 경우 이를 파일으로 간주하고, 파일 입력을 받아 전송해주는 경우
- 그냥 데이터를 정송하는 경우
- "/종료"를 입력할 경우 socket을 종료하는 경우

**수신 : receive**

```python
def receive(sock):
    while True:
        try:
            recvData = sock.recv(1024)
            data = recvData.decode('utf-8')
            if "    " in data:
                f = open(user + "_" + str(file_count) + "_file_received.txt", 'w')
                f.write(data.strip())
                f.close()
                print("파일을 저장했습니다.")
                data = data.split(":")
                if len(data) == 2:
                    user_name = data[0]
                    msg = data[1]
                continue
            else : 
                data = data.split(":")
                if len(data) == 2:
                    user_name = data[0]
                    msg = data[1]
                    print(user_name,':',msg)
                    continue

        except Exception as e:
            print("Error :", e)
            print(msg)
            sock.close()
            break
```

- 파일을 전송 받는 경우 시작이 "     "인 경우 파일을 저장하는 역할
- 만약 파일 전송이 아닌 그냥 메시지가 왔을 경우 누가 보낸 메시지인지 확인하기 위해
    - user_name과 msg를 ":" 기준으로 파싱하여 출력

### 스레딩을 통한 sender와 receiver 동시 실행

```python
sender = threading.Thread(target=send, args=(clientSock,))
receiver = threading.Thread(target=receive, args=(clientSock,))

sender.start()
receiver.start()
```

---

## 코드 전문

### server_side.py 코드 전문

```python
from threading import *
from socket import *

class ServerSocket:
 
    def __init__(self):        
        self.bListen = False       
        self.clients = []
        self.ip = []
        self.threads = []
        self.user_list = []

    def __del__(self):
        self.stop()

    def start(self, ip, port):
        ## 서버 소켓열고, ip와 포트 bind하고 스레드 열어주는 역할
        self.server = socket(AF_INET, SOCK_STREAM)            
 
        try:
            self.server.bind( (ip, port))
        except Exception as e:
            print('Bind Error : ', e)
            return False
        else:                 
            self.bListen = True
            self.t = Thread(target=self.listen, args=(self.server,))
            self.t.start()
            print('Server Listening...')
            
        return True

    def stop(self):
        ## 서버 끄는 역할
        self.bListen = False
        if hasattr(self, 'server'):            
            self.server.close()            
            print('Server Stop')

 
    def listen(self, server):
        ##새로 접속을 시도하는 유저를 받아주는 역할
        while self.bListen:
            server.listen(5)   
            try:
                client, addr = server.accept()
            except Exception as e:
                print('Accept() Error : ', e)
                break
            else:                
                user_dict = dict()
                user = client.recv(1024).decode('utf-8')

                self.clients.append(client)
                self.ip.append(addr)                
                t = Thread(target=self.receive, args=(addr, client))
                self.threads.append(t)
                user_dict['name'] = user
                user_dict['client'] = client
                user_dict['ip'] = addr
                user_dict['thread'] = t
                self.user_list.append(user_dict)
                t.start()   
                self.send("서버 : " + user + "님이 로그인하셨습니다.")
                self.resourceInfo()         
                 
        self.removeAllClients()
        self.server.close()

    def receive(self, addr, client):
        #유저가 보내는 메시지를 받아주는 역할
        while True:            
            try:
                recv = client.recv(1024)         
                msg = str(recv, encoding='utf-8')
                if msg =="/종료":
                    user_name = ""
                    for i in self.user_list:
                        if i['client'] == client:
                            user_name = i['name']
                            break
                    print(user_name + "가 로그아웃 하셨습니다.")
                    self.removeClient(addr, client)
                    self.send('서버 :'+ user_name + "가 로그아웃 하셨습니다.")
                    return
                    break
            except Exception as e:
                user_name = ""
                for i in self.user_list:
                    if i['client'] == client:
                        user_name = i['name']
                        break
                    
                self.removeClient(addr, client)
                self.send('서버 :'+ user_name + "가 비정상적으로 로그아웃 됐습니다.")
                print('Recv() Error :', e, addr)
                return
                break
            else:       
                if "    " in msg: 
                    self.send(msg)
                elif msg:
                    user_name = ""
                    for i in self.user_list:
                        if i['client'] == client:
                            user_name = i['name']
                            break
                    # if msg =="/종료":
                    #     print(user_name + "가 로그아웃 하셨습니다.")
                    #     self.send(user_name + "가 로그아웃 하셨습니다.")
                    #     self.removeClient(addr, client)
                    #     return
                    #     break

                    self.send(user_name+':'+str(msg))
                    print('[RECV From', str(user_name) +']' , addr, msg)
 
        self.removeClient(addr, client)

    def send(self, msg):
        #현재 존자하는 client들에게 메시지를 전송하는 역할
        try:
            for c in self.clients:
                c.send(msg.encode())
        except Exception as e:
            print('Send() Error : ', e)

    def removeClient(self, addr, client):
        #사용자가 비정상적으로 로그아웃되거나, 종료할 경우 로그아웃 시킴
        found = False
        for i in self.user_list:
            if i['client'] == client:
                user_name = i['name']
                self.ip.remove(addr)
                self.clients.remove(client) 
                thread = i['thread']
                i = 0
                for t in self.threads[:]:
                    if t == thread:
                        del(self.threads[i])
                    i +=1
                client.close()
                found = True
                break
        if not found : 
            print("error!!")
        

        self.resourceInfo()

    def removeAllClients(self):
        #모든 client들 다 지우는 역할
        for c in self.clients:
            c.close()
        self.ip.clear()
        self.clients.clear()
        self.threads.clear()
        self.resourceInfo()

    def resourceInfo(self):
        #현재 접속자 상황을 공유하는 역할
        print('Number of Client ip\t: ', len(self.ip) )
        print('Number of Client socket\t: ', len(self.clients) )
        print('Number of Client thread\t: ', len(self.threads) )
        # self.send('서버 :' + 'Number of Client ip -> ' + str(len(self.ip)))
        # self.send('서버 :' + 'Number of Client socket -> '+ str(len(self.clients)))
        # self.send('서버 :' + 'Number of Client thread -> '+ str(len(self.threads)))       

if __name__ == "__main__":
    ip = "127.0.0.1"
    port = 54321
    server = ServerSocket()
    server.start(ip, port)
```

### client_side.py

```python
from socket import *
import threading
import time
import os

def send(sock):
    while True:
        sendData = input('>>>')
        if sendData == "file":
            #파일 이름 입력
            while True : 
                try: 
                    file_name = input("파일 이름을 입력하세요")
                    #파일의 절대값 주소 생성
                    file_name = os.path.join(os.getcwd(), file_name)
                    f = open(file_name, 'r')
                    break
                except Exception as e : 
                    print("error! ", e)
            data = f.read()
            data = "    " + data
            double_check = input(file_name + "을 전송하시겠습니까? (Y/N)")
            if double_check.lower() == 'y':
                sock.send(data.encode('utf-8'))
                continue
            else:
                continue
        sock.send(sendData.encode('utf-8'))
        if sendData == "/종료":
            break
    sock.close()

def receive(sock):
    while True:
        try:
            recvData = sock.recv(1024)
            data = recvData.decode('utf-8')
            if "    " in data:
                f = open(user + "_" + str(file_count) + "_file_received.txt", 'w')
                f.write(data.strip())
                f.close()
                print("파일을 저장했습니다.")
                data = data.split(":")
                if len(data) == 2:
                    user_name = data[0]
                    msg = data[1]
                continue
            else : 
                data = data.split(":")
                if len(data) == 2:
                    user_name = data[0]
                    msg = data[1]
                    print(user_name,':',msg)
                    continue

        except Exception as e:
            print("Error :", e)
            print(msg)
            sock.close()
            break

port =54321
user = input("접속하고자 하는 이름을 입력해주세요 : ")
file_count = 0
clientSock = socket(AF_INET, SOCK_STREAM)
clientSock.connect(('127.0.0.1', port))
clientSock.sendall(user.encode())

print('접속 완료')

sender = threading.Thread(target=send, args=(clientSock,))
receiver = threading.Thread(target=receive, args=(clientSock,))

sender.start()
receiver.start()

while True:
    time.sleep(1)
    pass
```